## About Starter Laravel MySQL

It is a starter for web application development using:

- **[NGINX](https://www.nginx.com)**.
- **[Laravel](https://laravel.com)**.
- Supported whit **[Redis](https://redis.io/)**.


Any of the following database handlers can be used:
- **[MySQL](https://www.mysql.com)**.
- **[phpMyAdmin](https://www.phpmyadmin.net/)**.

Or

- **[PostgreSQL](https://www.postgresql.org/)**.
- **[PgAdmin](https://www.pgadmin.org/)**.

## Installation

### Docker

Go to the ``.docker`` folder.

```
$ cp .env.example .env
```

For use **MySQL** copy the file ``.env.mysql`` to ``.env`` and ``compose.mysql`` to ``docker-compose.yml``.

```
$ cp .env.mysql .env
$ cp compose.mysql docker-compose.yml
```

For use **PostgreSQL** copy the file ``.env.psql`` to ``.env`` and ``compose.psql`` to ``docker-compose.yml``.

```
$ cp .env.psql .env
$ cp compose.psql docker-compose.yml
```

In the **.env** file we can customize the variables. If will use redis for the use of the cache change ``ENV_CACHE_DRIVER=file`` for ``ENV_CACHE_DRIVER=redis``.

Initialize docker containers.

```
$ docker-compose up -d
```

**NOTE:** It is recommended to make a backup of the file ``.env`` to ``.env.example`` and delete the files ``.env.mysql``, ``.env.psql``, ``compose.mysql`` and ``compose.psql``.

```
$ cp .env .env.example
$ rm .env.mysql
$ rm .env.psql
$ rm compose.mysql
$ rm compose.psql
```
### Laravel

Enter to bash of application.

```
$ docker exec -it php_starter bash
```

Copy the file ``.env.example`` to ``.env``. Now install laravel dependencies.

```
$ cp .env.example .env
$ composer install
```

Generate the key of the laravel application.

```
$ php artisan key:generate
```

Congratulations! You have your development environment set up and ready to go.

## Usage in browser

### For MySQL

To start the application go to ``http://localhost``. To access phpMyAdmin enter ``http://localhost:8000`` 
with username ``root`` and password ``root_secret``. The name of the database is ``laravel``.

### For PostgreSQL

To start the application go to ``http://localhost``. To access pgAdmin enter ``http://localhost:8000`` 
with username ``root@localhost.com`` and password ``admin_pg``.

Create a new server using:
- **Ip Address**:execute from the main bash ``docker inspect postgres_starter`` and copy **IPAddress**
- **Port:** 5432
- **Database:** laravel
- **User:** admin_db
- **Password:** secret

## License

This starter kit is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
